/* paramsdialog.cpp */
/* part of DAta REScue dares */

/* DAta REScue (C) 2002 Oliver Diedrich, odi@ct.heise.de */
/* (C) 2005 c't magazine for computer technology, Heise Zeitschriften Verlag */
/* This file may be redistributed under the terms of the */
/* GNU Public License (GPL), see www.gnu.org */

#ifndef PARAMS_DIALOG_H
#define PARAMS_DIALOG_H

#include <qdialog.h>

class QLineEdit;
class QCheckBox;


class ParamsDialog: public QDialog
{
  Q_OBJECT
public:
  ParamsDialog( const QString& imageName,
                const QString& savePath,
                const QString& h2iFileName,
                const QString& logName,
                bool useMIME,
                QWidget *parent = 0,
                const char *name = 0,
                bool modal = TRUE,
                WFlags f = 0 );
  ~ParamsDialog() {}

  QString mImageName;
  QString mSavePath;
  QString mH2iFileName;
  QString mLogName;
  bool    mUseMIME;

protected slots:
  void selectImageName();
  void selectSavePath();
  void selectH2iFileName();
  void selectLogFile();
  void accept();

private:
  QLineEdit* mEditImageName;
  QLineEdit* mEditSavePath;
  QLineEdit* mEditH2iFileName;
  QLineEdit* mEditLogName;
  QCheckBox* mCheckUseMIME;
};

#endif
