/* DaResView.h */
/* part of DAta REScue dares */

/* DAta REScue (C) 2002 Oliver Diedrich, odi@ct.heise.de */
/* (C) 2005 c't magazine for computer technology, Heise Zeitschriften Verlag */
/* This file may be redistributed under the terms of the */
/* GNU Public License (GPL), see www.gnu.org */

#ifndef __DARES_VIEW_H
#define __DARES_VIEW_H

#include <qsplitter.h>
#include <qstring.h>
#include <qobject.h>
#include <qdatetime.h>
#include <qptrlist.h>
#include <qlistview.h>
#include <qdict.h>

class QListView;
class QTextEdit;
class QLabel;
class QPainter;
class QColorGroup;
class QObjectList;
class QPopupMenu;


// -----------------------------------------------------------------

class RawFile
{
public:
  RawFile(QWidget* parent ) : mParent( parent ), mDaresFileInfo( 0 ){}

  RawFile( QWidget* parent, struct one_file *daresFileInfo, const QCString& daresTypeName )
    : mParent( parent ),
      mDaresFileInfo( daresFileInfo ),
      mDaresTypeName( daresTypeName ){}

  RawFile( const RawFile &m )
    : mParent(        m.mParent ),
      mDaresFileInfo( m.mDaresFileInfo ),
      mDaresTypeName( m.mDaresTypeName ),
      mSavedFileName( m.mSavedFileName ) {}

  QString content();
  QString info();
  bool hasFileInfo();
  bool saveToFile(bool bSilentOnSuccess);
  QCString daresTypeName();
  QCString savedFileName();
  struct one_file* daresFileInfo();

protected:
  QWidget* mParent;
  struct one_file* mDaresFileInfo;
  QCString mDaresTypeName;
  QCString mSavedFileName;
};


// -----------------------------------------------------------------

class Folder : public QObject
{
    Q_OBJECT

public:
    Folder( Folder *parent, const QString &name, const RawFile& rawFile );
    ~Folder() {}

    QString folderName() { return mFName; }

    RawFile& rawFile() { return mRawFile; }

protected:
    QString mFName;
    RawFile mRawFile;
};


// -----------------------------------------------------------------

class FolderListItem : public QListViewItem
{
public:
    FolderListItem( QListView *parent, Folder *f );
    FolderListItem( FolderListItem *parent, FolderListItem* prev, Folder *f );
    FolderListItem( FolderListItem *parent, Folder *f );

    void insertSubFolders( const QObjectList *lst );

    Folder *folder() { return myFolder; }

protected:
    Folder *myFolder;

};


// -----------------------------------------------------------------

class DaResView : public QSplitter
{
    Q_OBJECT

public:
    DaResView( QWidget *parent = 0, const char *name = 0, int found = 0 );
    ~DaResView() {}

protected:
    void initFolders();
    void setupFolders();

    QListView*  mFolders;
    QTextEdit*  mRawFileView;
    QLabel*     mRawFileLabel;
    QPopupMenu* mPopUpMenu;

    QPtrList<Folder> lstFolders;

protected slots:
    void slotFolderChanged( QListViewItem* );
    void slotRMB( QListViewItem*, const QPoint &, int );
    void updateRawFileView( FolderListItem *i );
    void viewCurrent();
    void saveCurrent();
    void saveAllSelected();

protected:
    bool saveItem( QListViewItem* _item, bool bSilentOnSuccess, bool restoreStatusLabel=true );
    bool hasFileInfo( QListViewItem *i );

private:
    int mFound;
    QDict<QString> programs;
};

#endif
