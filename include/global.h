/* dares.h */
/* part of DAta REScue dares */

/* DAta REScue (C) 2002 Oliver Diedrich, odi@ct.heise.de */
/* (C) 2005 c't magazine for computer technology, Heise Zeitschriften Verlag */
/* This file may be redistributed under the terms of the */
/* GNU Public License (GPL), see www.gnu.org */

#ifndef __DARES_GLOBAL_H__
#define __DARES_GLOBAL_H__

#define VERSION "dares 0.6.5"

#endif
